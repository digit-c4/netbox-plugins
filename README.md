# Netbox Europa Distribution

## Requirement

- docker
- docker-compose

## Contribute

Clone this project:

```
git clone git@code.europa.eu:digit-c4/netbox-plugins.git
```

### On Window

If you start from scratch:

Create SSH key:
[instruction](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key)

Add key.pub to the git account:
[instruction](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)

Then clone the project.

#### If you are behind a proxy

create file `%USERPROFILE%/.ssh/config` and add below values:

```
Host code.europa.eu
    ProxyCommand "C:/Program Files/Git/mingw64/bin/connect.exe" -H proxy-t2.welcome.ec.europa.eu:8012 %h %p
```

Create local environment variables:

| Variable | Value |
|----------| ------|
|HTTP_PROXY_USER | user |
|HTTP_PROXY_PASSWORD | password |

##  Run localy

### on Linux

sudo TAG=latest docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d

### on Mac

env TAG=latest docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d

### Login to Netbox

url : http://localhost:8080
login: admin
password: thisnetboxisnot4u
