
# Creating a Webhook in NetBox Instance Documentation

Webhooks in NetBox provide a powerful mechanism to trigger custom actions or workflows when certain events occur within the system. This documentation outlines the process of creating a webhook in a NetBox instance to generate default entries, such as RPS HTTP Headers, when inserting data even in a different context, such as RPS Mappings.

## Prerequisites
Before creating a webhook in your NetBox instance, ensure you have the following:
1.	Understanding of Webhooks: Familiarity with the concept of webhooks and how they work is recommended.
2.	Understanding of NetBox Models: Knowledge of NetBox data models, especially the ones relevant to the context where you want to insert data, is necessary.

## Steps to Create a Webhook
Follow these steps to create a Webhook in your NetBox instance:

#### 1. Create the API token
Click on your username on the top right corner of the screen and select API Token.
 
#### 2. Create a new token
Fill the form in order to create a token and store it for later use.
 

#### 3. Navigate to Webhooks Section
Navigate to the “Operations” menu option and click on "Webhooks".
 

#### 4. Add a new Webhook
Click on the green button labelled "+Add" to add a new Webhook.


#### 5. Fill the form
Fill the details of your Webhook:


##### Webhook Section

Name: Name of your Webhook.    
Content Types: Content that will be used as a primary trigger. For example: "Netbox RPS > Mapping".    
Enable Flag: Click if you want to enable your Webhook.     
Events: Which events will be used to trigger your Webhook.    


##### HTTP Request Section
 
URL: This URL will be called using the HTTP method defined when the webhook is called.    
HTTP Method: Which method will be used.    
HTTP Content type: application/json.    
Additional headers: Get the Authorization API Token that you created earlier in this tutorial.    
Body template: Jason template for the object you want to create when the selected event is triggered.    

For example, HTTP Headers:
```
{
    "name": "X-Forwarded-Proto", 
    "value": "request.x_header.X-Forwarded-Proto $(CLIENT.PROTOCOL)", 
    "apply_to": "request",
    "mapping": {"id": "{{ data['id'] }}"}
}
```

or multiple objects, multiple HTTP Headers:

```
[
{
    "name": "X-Forwarded-Proto", 
    "value": "request.x_header.X-Forwarded-Proto $(CLIENT.PROTOCOL)", 
    "apply_to": "request",
    "mapping": {"id": "{{ data['id'] }}"}
},
{
    "name": "client-IP", 
    "value": "request.header.client-IP $(client.address)", 
    "apply_to": "request",
    "mapping": {"id": "{{ data['id'] }}"}
},
{
    "name": "Forwarded-For", 
    "value": "request.header.X-Forwarded-For $(client.effective_address)", 
    "apply_to": "request",
    "mapping": {"id": "{{ data['id'] }}"}
},
{
    "name": "Forwarded-Host", 
    "value": "request.x_header.X-Forwarded-Host $(url.host)", 
    "apply_to": "request",
    "mapping": {"id": "{{ data['id'] }}"}
}
]
```

#### 6. Save the webhook
Then you can click the Save Button
