FIELD_CHOICES = {
    'netbox_dns.Record.status+':( 
        ('planned', 'Planned', 'grey'), 
        ('failed', 'Failed', 'red'),
        ('deployed', 'Deployed', 'green')
  )
}