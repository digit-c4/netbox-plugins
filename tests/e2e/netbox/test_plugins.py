"""Test case for Netbox Installed Plugins"""

import unittest
import os
import json
import requests

HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_TOKEN = os.getenv("API_TOKEN", default="only4testingpurpose")
HEADERS = {"Authorization": f"Token {API_TOKEN}"}
PLUGINS = [
    "netbox_dns",
    "netbox_rps_plugin",
    "netbox_cert_plugin",
    "netbox_prometheus_sd",
    "netbox_docker_plugin",
    "netbox_mac_address_plugin",
]


class TestNetboxPluginInstalled(unittest.TestCase):
    """Test case for Netbox installed plugins"""

    def test_that_netbox_plugins_are_installed(self) -> None:
        """Test that Netbox Plugins are installed"""

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/installed-plugins/",
            timeout=5,
            headers=HEADERS,
        )

        self.assertEqual(response.status_code, 200)

        packages = json.loads(response.content)

        for plugin in PLUGINS:
            self.assertNotEqual(
                next(
                    (package for package in packages if package["package"] == plugin),
                    None,
                ),
                None,
                f'Plugin "{plugin}" not found',
            )


if __name__ == "__main__":
    unittest.main()
