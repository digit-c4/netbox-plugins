"""Test case for Certificate creation"""

import time
import unittest
import json
import os
import requests


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestCertificateCreation(unittest.TestCase):
    """Test case for Mapping creation class"""

    mapping_id = None

    def test_that_certificate_is_created(self) -> None:
        """Test that certificate is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/CertificateAuthority/",
            json={
                "ca_name": "letsencrypt_test"
                },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_default_certificate_properties_is_set(self) -> None:
        """Test that default certificate properties is set"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/CertificateAuthority/",
            json={
                "ca_name": "letsencrypt_test", 
                "acme_url": "www.letsencrypt.com",
                "key_vault_url":"www.key_vault_url.com"
                },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        content = json.loads(response.content)
        self.assertEqual(content["ca_name"], "letsencrypt_test")
        self.assertEqual(content["acme_url"], "www.letsencrypt.com")
        self.assertEqual(content["key_vault_url"], "www.key_vault_url.com")

    def test_that_cn_is_unique(self) -> None:
        """Test that CN is unique"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/CertificateAuthority/",
            json={"ca_name": "letsencrypt_test"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/CertificateAuthority/",
            json={"ca_name": "letsencrypt_test"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"ca_name":["certificate authority with this CA Name already exists."]}',
        )


    def test_that_valid_from_is_before_valid_until(self) -> None:
        """Test that CN is unique"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/CertificateAuthority/",
            json={"ca_name": "letsencrypt_test","start_date":"2024-10-14","end_date":"2024-09-14"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            # pylint: disable=C0301
            b'{"__all__":["Constraint \xe2\x80\x9cThe end date must be after the start date\xe2\x80\x9d is violated."]}',
        )


    def tearDown(self) -> None:
        """Teardown function"""

        requests.delete(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/CertificateAuthority/",
            json=[{"id": self.mapping_id}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        time.sleep(2)


if __name__ == "__main__":
    unittest.main()
