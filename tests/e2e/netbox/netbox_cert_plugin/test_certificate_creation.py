"""Test case for Certificate creation"""

import time
import unittest
import json
import os
import requests


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestCertificateCreation(unittest.TestCase):
    """Test case for Mapping creation class"""

    mapping_id = None

    def test_that_certificate_is_created(self) -> None:
        """Test that certificate is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={"cn": "truc00.com", "ca": "letsencrypt", "expiration_time": "1m"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_default_certificate_properties_is_set(self) -> None:
        """Test that default certificate properties is set"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={"cn": "truc00.com", "ca": "letsencrypt"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        content = json.loads(response.content)
        self.assertEqual(content["expiration_time"], "1m")
        self.assertEqual(content["state"], "to_be_created")

    def test_that_cn_is_unique(self) -> None:
        """Test that CN is unique"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={"cn": "truc00.com", "ca": "letsencrypt", "expiration_time": "1m"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={"cn": "truc00.com", "ca": "commissign", "expiration_time": "3m"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"cn":["certificate with this Common Name already exists."]}',
        )

    def test_that_ca_is_valid(self) -> None:
        """Test that CA is valid"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={"cn": "truc00.com", "ca": "randomca", "expiration_time": "3m"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"ca":["\\"randomca\\" is not a valid choice."]}'
        )

    def test_that_expiration_time_is_valid(self) -> None:
        """Test that expiration time is valid"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={"cn": "truc00.com", "ca": "commissign", "expiration_time": "10m"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"expiration_time":["\\"10m\\" is not a valid choice."]}',
        )

    def test_that_alt_name_is_an_array(self) -> None:
        """Test that Alt name is an array"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={
                "cn": "truc00.com",
                "ca": "letsencrypt",
                "expiration_time": "1m",
                "alt_name": "truc01.com",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"alt_name":["Expected a list of items but got type \\"str\\"."]}',
        )

    def test_that_certificate_is_created_with_alt_name(self) -> None:
        """Test that certificate is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json={
                "cn": "truc00.com",
                "ca": "commissign",
                "expiration_time": "1m",
                "alt_name": ["192.168.1.1", "truc01.com"],
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

    def tearDown(self) -> None:
        """Teardown function"""

        requests.delete(
            url=f"http://{HOST}:{PORT}/api/plugins/cert/certificate/",
            json=[{"id": self.mapping_id}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        time.sleep(2)


if __name__ == "__main__":
    unittest.main()
