"""Test case for Netbox Metrics"""

import unittest
import os
import requests


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")


class TestNetboxMetrics(unittest.TestCase):
    """Test case for Netbox Metrics"""

    def test_that_netbox_metrics_is_activated(self) -> None:
        """Test that Netbox Metrics is activated"""

        response = requests.get(
            url=f"http://{HOST}:{PORT}/metrics",
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
