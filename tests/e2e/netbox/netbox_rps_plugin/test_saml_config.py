"""Test case for Mapping SAML configuration"""

import json
import unittest
import os
import requests
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestSamlConfig(Base):
    """Test case for Mapping SAML configuration class"""

    mapping_id = None

    def test_that_saml_config_is_created(self) -> None:
        """Test that SAML config is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc7.com/api",
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost",
                "logout_url": "http://localhost",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        saml_config_id = json.loads(response.content)["id"]

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/{self.mapping_id}/",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertIsNotNone(json.loads(response.content)["saml_config"])
        self.assertEqual(
            json.loads(response.content)["saml_config"]["id"], saml_config_id
        )

    def test_that_saml_config_is_cascade_deleted(self) -> None:
        """Test that SAML config is cascade deleted"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc7.com/api",
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost",
                "logout_url": "http://localhost",
                "force_nauth": False,
                "mapping": mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        saml_config_id = json.loads(response.content)["id"]

        requests.delete(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json=[{"id": mapping_id}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/{saml_config_id}/",
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 404)

    def test_that_patch_works(self) -> None:
        """Test that PATCH HTTP request works"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/".format(HOST, PORT),
            json={
                "source": "https://truc7.com/api",
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost",
                "logout_url": "http://localhost",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        saml_config_id = json.loads(response.content)["id"]

        response = requests.patch(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/{saml_config_id}/",
            json={"acs_url": "http://anotherhost.com:8080"},
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/{saml_config_id}/",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            json.loads(response.content)["acs_url"], "http://anotherhost.com:8080"
        )

    def test_that_acs_url_is_an_url(self) -> None:
        """Test that ACS URL is an URL"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc7.com/api",
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://thisisanurl",
                "logout_url": "http://localhost",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"acs_url":["It must be a url"]}')

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "verynotanurl",
                "logout_url": "http://localhost",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"acs_url":["It must be a url"]}')

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost/api" + ("i" * 1981),
                "logout_url": "http://localhost",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"acs_url":["Ensure this field has no more than 2000 characters."]}',
        )

    def test_that_logout_url_is_an_url(self) -> None:
        """Test that logout URL is an URL"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc7.com/api",
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost",
                "logout_url": "http://thisisanurl",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"logout_url":["It must be a url"]}')

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost",
                "logout_url": "verynotanurl",
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"logout_url":["It must be a url"]}')

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/saml_config/",
            json={
                "acs_url": "http://localhost/api",
                "logout_url": "http://localhost/api" + ("i" * 1981),
                "force_nauth": False,
                "mapping": self.mapping_id,
            },
            headers={
                "Authorization": f"Token {API_KEY}",
                "accept": "application/json",
            },
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"logout_url":["Ensure this field has no more than 2000 characters."]}',
        )


if __name__ == "__main__":
    unittest.main()
