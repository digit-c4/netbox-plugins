"""Test case for Mapping authentication"""

import unittest
import os
import requests


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestUnauthenticatedMappings(unittest.TestCase):
    """Test case for Mapping authentication class"""

    def test_mappings_get_unauthenticated(self) -> None:
        """Test that mappings GET authentication"""

        response = requests.get(
            f"http://{HOST}:{PORT}/api/plugins/rps/mapping/", timeout=5
        )

        self.assertEqual(response.status_code, 403)

    def test_mappings_post_unauthenticated(self) -> None:
        """Test that mappings POST authentication"""

        response = requests.post(
            f"http://{HOST}:{PORT}/api/plugins/rps/mapping/", timeout=5
        )

        self.assertEqual(response.status_code, 403)

    def test_mappings_patch_unauthenticated(self) -> None:
        """Test that mappings PATCH authentication"""

        response = requests.patch(
            f"http://{HOST}:{PORT}/api/plugins/rps/mapping/", timeout=5
        )

        self.assertEqual(response.status_code, 403)

    def test_mappings_delete_unauthenticated(self) -> None:
        """Test that mappings DELETE authentication"""

        response = requests.delete(
            f"http://{HOST}:{PORT}/api/plugins/rps/mapping/", timeout=5
        )

        self.assertEqual(response.status_code, 403)


if __name__ == "__main__":
    unittest.main()
