"""Base TestCase Class for Mapping"""

import os
import time
import unittest
import requests


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class Base(unittest.TestCase):
    """Base TestCase Class for Mapping"""

    mapping_id = None

    def tearDown(self) -> None:
        """Teardown function"""

        if self.mapping_id is not None:
            requests.delete(
                url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
                json=[{"id": self.mapping_id}],
                headers={"Authorization": f"Token {API_KEY}"},
                timeout=5,
            )

        time.sleep(2)
