"""Test case for Mapping is unique"""

import unittest
import json
import os
import requests
from .base import Base

HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingUnique(Base):
    """Test case for Mapping is unique class"""

    mapping_id = None

    def test_that_mapping_is_unique(self) -> None:
        """Test that mapping is unique"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc8.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc8.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"source":["mapping with this Source already exists."]}'
        )

    def test_that_source_url_cannot_be_equal_to_destination_url(self) -> None:
        """Test that source ULR cannot be equal to destination URL"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc9.com/api",
                "target": "https://truc9.com/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"target":["Target URL cannot be equal to source URL."]}',
        )

    def test_that_mapping_is_case_sensitive_unique(self) -> None:
        """Test that mapping is case sensitive unique"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc10.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "HTTPS://truc10.com/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"source":["Mapping with this Source already exists."]}'
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://TRUC10.COM/api",
                "target": "http://10.10.10.10:1888/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content, b'{"source":["Mapping with this Source already exists."]}'
        )

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "HTTPS://TRUC10.com/API",
                "target": "HTTP://Toto.10.com:1888/aPi",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://truc10.com/API")
        self.assertEqual(content["target"], "http://toto.10.com:1888/aPi")

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/{content['id']}/",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://truc10.com/API")
        self.assertEqual(content["target"], "http://toto.10.com:1888/aPi")

        requests.delete(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json=[{"id": content["id"]}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

    def test_that_mapping_update_is_case_sensitive_unique(self) -> None:
        """Test that mapping update is case sensitive unique"""
        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "HTTPS://TRUC11.com/API",
                "target": "HTTP://Toto.11.com:1888/aPi",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        content = json.loads(response.content)

        response = requests.patch(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/{content['id']}/",
            json={
                "source": "HTTPS://MUCHE11.com/API",
                "target": "HTTP://Titi.11.com:1888/aPi",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://muche11.com/API")
        self.assertEqual(content["target"], "http://titi.11.com:1888/aPi")

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/{content['id']}/",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEqual(content["source"], "https://muche11.com/API")
        self.assertEqual(content["target"], "http://titi.11.com:1888/aPi")

        requests.delete(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json=[{"id": content["id"]}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

if __name__ == "__main__":
    unittest.main()
