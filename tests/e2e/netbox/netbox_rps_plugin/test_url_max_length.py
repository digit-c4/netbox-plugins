"""Test case for Mapping URL max length """

import unittest
import os
import requests
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingUrlMaxLength(Base):
    """Test case for Mapping URL max length class"""

    def test_that_source_url_has_max_length(self) -> None:
        """Test that source URL has max length"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc.com/api" + ("i" * 1981),
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"source":["Ensure this field has no more than 2000 characters."]}',
        )

    def test_that_target_url_has_max_length(self) -> None:
        """Test that target URL has max length"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc.com/api",
                "target": "http://10.10.10.10:1886/api" + ("i" * 1974),
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"target":["Ensure this field has no more than 2000 characters."]}',
        )

    def test_that_testingpage_url_has_max_length(self) -> None:
        """Test that testing page URL has max length"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc.com/api",
                "target": "http://10.10.10.10:1886/api",
                "authentication": "none",
                "testingpage": "https://truc.com/api" + ("i" * 1981),
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content,
            b'{"testingpage":["Ensure this field has no more than 2000 characters."]}',
        )


if __name__ == "__main__":
    unittest.main()
