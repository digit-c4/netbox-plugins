"""Test case for Mapping's Headers creation"""

import time
import unittest
import json
import os
import requests
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingHeadersCreation(Base):
    """Test case for Mapping's Headers creation class"""

    first_mapping_id = None

    second_mapping_id = None

    def test_that_mapping_headers_is_created(self) -> None:
        """Test that mapping is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.first_mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc01.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.second_mapping_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/http_header/",
            json=[
                {
                    "name": "test1",
                    "value": "value1",
                    "mapping": {"id": self.first_mapping_id},
                },
                {
                    "name": "test2",
                    "value": "value2",
                    "mapping": {"id": self.first_mapping_id},
                },
                {
                    "name": "test3",
                    "value": "value3",
                    "mapping": {"id": self.second_mapping_id},
                },
            ],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/http_header/?mapping={self.second_mapping_id}",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEqual(content["count"], 1)
        self.assertEqual(content["results"][0]["mapping"]["id"], self.second_mapping_id)
        self.assertEqual(content["results"][0]["name"], "test3")
        self.assertEqual(content["results"][0]["value"], "value3")

    def tearDown(self) -> None:# pylint: disable=invalid-name
        """Teardown function"""

        if self.first_mapping_id is not None:
            requests.delete(
                url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
                json=[{"id": self.first_mapping_id}],
                headers={"Authorization": f"Token {API_KEY}"},
                timeout=5,
            )

        if self.second_mapping_id is not None:
            requests.delete(
                url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
                json=[{"id": self.second_mapping_id}],
                headers={"Authorization": f"Token {API_KEY}"},
                timeout=5,
            )

        time.sleep(2)


if __name__ == "__main__":
    unittest.main()
