"""Test case for Mapping creation"""

import unittest
import json
import os
import requests
from .base import Base


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMappingCreation(Base):
    """Test case for Mapping creation class"""

    mapping_id = None

    def test_that_mapping_is_created(self) -> None:
        """Test that mapping is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
                "authentication": "none",
                "testingpage": None,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

    def test_that_properties_mapping_default_value_is_set(self) -> None:
        """Test that properties mapping default value is set"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/rps/mapping/",
            json={
                "source": "https://truc00.com/api",
                "target": "http://10.10.10.10:1800/api",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mapping_id = json.loads(response.content)["id"]

        content = json.loads(response.content)

        self.assertEqual(content["authentication"], "none")
        self.assertEqual(content["extra_protocols"], [])


if __name__ == "__main__":
    unittest.main()
