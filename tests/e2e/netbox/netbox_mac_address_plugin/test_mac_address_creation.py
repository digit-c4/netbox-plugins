"""Test case for Mac Address creation"""

import time
import unittest
import json
import os
import requests


HOST = os.getenv("HOST", default="localhost")
PORT = os.getenv("PORT", default="8080")
API_KEY = os.getenv("API_KEY", "only4testingpurpose")


class TestMacAddressCreation(unittest.TestCase):
    """Test case for Mac Address creation class"""

    mac_address_id = None
    vlan_group_id = None
    vlan_id = None

    def test_that_mac_address_is_created(self) -> None:
        """Test that Mac Address is created"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/ipam/vlan-groups/",
            json={
                "name": "Test VLAN Group",
                "slug": "test_vlan_group",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.vlan_group_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/",
            json={
                "mac_address": "A2:F1:EA:F0:1D:03",
                "status": "active",
                "description": "description 3",
                "end_date": "2024-11-01",
                "assigned_object_type": "vlangroup", 
                "assigned_object_id": self.vlan_group_id,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mac_address_id = json.loads(response.content)["id"]

    def test_that_mac_address_is_unique(self) -> None:
        """Test that Mac Address is unique"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/ipam/vlan-groups/",
            json={
                "name": "Test VLAN Group",
                "slug": "test_vlan_group",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.vlan_group_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/",
            json={
                "mac_address": "A2:F1:EA:F0:1D:03",
                "status": "active",
                "description": "description 3",
                "end_date": "2024-11-01",
                "assigned_object_type": "vlangroup", 
                "assigned_object_id": self.vlan_group_id,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mac_address_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/",
            json={
                "mac_address": "A2:F1:EA:F0:1D:03",
                "status": "inactive",
                "description": "description new",
                "end_date": "2024-09-01",
                "assigned_object_type": "vlangroup", 
                "assigned_object_id": self.vlan_group_id,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)

        self.assertEqual(
            response.content,
            b'{"mac_address":["Mac Address with this mac address already exists."]}',
        )

    def test_that_mac_address_can_be_assigned_to_vlangroup(self) -> None:
        """Test that Mac Address is created assigned to a vlan group"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/ipam/vlan-groups/",
            json={
                "name": "Test VLAN Group",
                "slug": "test_vlan_group",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.vlan_group_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/",
            json={
                "mac_address": "A2:F1:EA:F0:1D:03",
                "status": "active",
                "description": "description 3",
                "end_date": "2024-11-01",
                "assigned_object_type": "vlangroup", 
                "assigned_object_id": self.vlan_group_id,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mac_address_id = json.loads(response.content)["id"]

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/{self.mac_address_id}/",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            json.loads(response.content)["vlan_groups"]["id"], self.vlan_group_id
        )

    def test_that_mac_address_can_be_assigned_to_vlan(self) -> None:
        """Test that Mac Address is created assigned to a vlan """

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/ipam/vlans/",
            json={
                "name": "Test VLAN",
                "slug": "test_vlan",
                "vid": 100,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.vlan_id = json.loads(response.content)["id"]

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/",
            json={
                "mac_address": "A2:F1:EA:F0:1D:03",
                "status": "active",
                "description": "description 3",
                "end_date": "2024-11-01",
                "assigned_object_type": "vlan", 
                "assigned_object_id": self.vlan_id,
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 201)

        self.mac_address_id = json.loads(response.content)["id"]

        response = requests.get(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/{self.mac_address_id}/",
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            json.loads(response.content)["vlans"]["id"], self.vlan_id
        )

    def test_that_mac_address_has_to_be_assigned_to_vlan_or_vlangroup(self) -> None:
        """Test that Mac Address has to be assigned to a vlan or a vlan group"""

        response = requests.post(
            url=f"http://{HOST}:{PORT}/api/plugins/mac-address/macaddress/",
            json={
                "mac_address": "A2:F1:EA:F0:1D:03",
                "status": "active",
                "description": "description 3",
                "end_date": "2024-11-01",
            },
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        self.assertEqual(response.status_code, 400)

        self.assertEqual(
            response.content,
            b'{"assigned_object_id":["This field is required."],"assigned_object_type":["This field is required."]}',
        )

    def tearDown(self) -> None:
        """Teardown function"""

        super().tearDown()

        requests.delete(
            url=f"http://{HOST}:{PORT}/api/plugins/plugins_netbox_mac_address/macaddresses/",
            json=[{"id": self.mac_address_id}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
        )

        if self.vlan_group_id:
            requests.delete(
            url=f"http://{HOST}:{PORT}/api/ipam/vlan-groups/",
            json=[{"id": self.vlan_group_id}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
            )
            self.vlan_group_id = None

        if self.vlan_id:
            requests.delete(
            url=f"http://{HOST}:{PORT}/api/ipam/vlans/",
            json=[{"id": self.vlan_id}],
            headers={"Authorization": f"Token {API_KEY}"},
            timeout=5,
            )
            self.vlan_id = None

        time.sleep(2)


if __name__ == "__main__":
    unittest.main()
