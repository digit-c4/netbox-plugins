# Testing the plugin

## Prepare a Python environment for E2E and Robot tests

```shell
python3 -m venv venv
source venv/bin/activate
pip install -r tests/requirements.e2e.txt
pip install -r tests/requirements.robot.txt
```

## E2E, End to end

```shell
python -m unittest discover tests/e2e
```
or
```shell
env HOST=<host_to_connect> PORT=8080 API_TOKEN=<netbox_token> python -m unittest discover tests/e2e
```


## Run Robot Framework tests
###
1. Start the netbox instance:
```sudo TAG=latest docker-compose -f docker-compose.yml -f docker-compose.test.yml up -d ```
###
2. Check in which PORT Netbox is running.
###
3. Go to the test folder and change the value of the variable `PORT` in the varibales.yaml file to the correct port number used by netbox
###
4. Run the tests:
```
robot ./tests/robot/
```
or
```
robot -v HOST:<host_to_connect> -v PORT:8080 -v API_TOKEN:<netbox_token> ./tests/robot
```
###
5. Open the file `report.html` in the browser to check the test results.
