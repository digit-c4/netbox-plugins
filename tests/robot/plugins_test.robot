*** Settings ***
Test Tags  core
Library  RequestsLibrary
Variables    variables.yaml

*** Variables ***
${BASE_URL}  http://${HOST}:${PORT}
${TOKEN}    Token ${API_TOKEN}

*** Test Cases ***

Check installed plugins
    ${HEADERS}       Create Dictionary    Content-Type    application/json    Authorization    ${TOKEN} 
    Log to console    ${\n}

    FOR    ${item}    IN    @{PLUGINS}
      ${response}=  GET  ${BASE_URL}/api/plugins/${item}/  headers=${HEADERS}  expected_status=200
      Check plugins response status "${response.ok}" "${item}"
    END
    Log to console    ${\n}
    
Check Nonexistent Plugin
    ${HEADERS}       Create Dictionary    Content-Type    application/json    Authorization    ${TOKEN} 
    Log to console    ${\n}
    ${response}=  GET  ${BASE_URL}/api/plugins/nonexistent_plugin/  headers=${HEADERS}  expected_status=404
    Check plugins response status "${response.ok}" "nonexistent"
    

*** Keywords ***

Check plugins response status "${response.ok}" "${item}"
    IF    "${response.ok}" == "True"
        Log to console    ${\n}Plugin ${item} Installed
        Log    ${\n}Plugin ${item} Installed
    ELSE
        Log to console    ${\n}Plugin ${item} Not Installed
        Log    ${\n}Plugin ${item} Not Installed
    END
