*** Settings ***
Test Tags  core
Library  RequestsLibrary
Variables    variables.yaml

*** Variables ***
${BASE_URL}  http://${HOST}:${PORT}
${TOKEN}    Token ${API_TOKEN}

*** Test Cases ***

Check Metrics Page
    ${response}=    GET    ${BASE_URL}/metrics    expected_status=200
    Log to console    ${\n}
    Check metrics response status "${response.ok}" "metrics"
    Log to console    ${\n}

Check Nonexistent Page
    ${HEADERS}       Create Dictionary    Content-Type    application/json    Authorization    ${TOKEN} 
    Log to console    ${\n}
    ${response}=    GET    ${BASE_URL}/nonexisting_page    headers=${HEADERS}  expected_status=404
    Log to console    ${\n}
    Check metrics response status "${response.ok}" "nonexistent"
    Log to console    ${\n}

*** Keywords ***

Check metrics response status "${response.ok}" "${PAGE}"
    IF    "${response.ok}" == "True"
        Log to console    ${\n}${PAGE} Ready
        Log    ${\n}${PAGE} Ready
    ELSE
        Log to console    ${\n}${PAGE} Not Ready
        Log    ${\n}${PAGE} Not Ready
    END
