*** Settings ***
Test Tags  mac
Library  RequestsLibrary
Library    OperatingSystem    
Library    Collections
Variables    ../variables.yaml

*** Variables ***
${BASE_URL}       http://${HOST}:${PORT}
${MAC_ENDPOINT}      /api/plugins/mac-address/macaddress/
${VLAN_ENDPOINT}    /api/ipam/vlans/
${VLANGROUP_ENDPOINT}    /api/ipam/vlan-groups/
${TOKEN}    Token ${API_TOKEN}

*** Test Cases ***

Create VLAN for testing
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${payload}       Create Dictionary    name    robottest_vlan    vid    2
    ${response}=  POST  ${BASE_URL}${VLAN_ENDPOINT}    json=${payload}    headers=${HEADERS}  expected_status=201
    ${json}  Set Variable    ${response.json()}
    Convert to Dictionary    ${json}
    ${VLAN_ID}    Get From Dictionary    ${json}    id
    Set Global Variable    ${VLAN_ID}


Create VLAN Group for testing 
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${payload}       Create Dictionary    name    robottest_vlan_group    slug    slug_test
    ${response}=  POST  ${BASE_URL}${VLANGROUP_ENDPOINT}    json=${payload}    headers=${HEADERS}  expected_status=201
    ${json}  Set Variable    ${response.json()}
    Convert to Dictionary    ${json}
    ${VLANGROUP_ID}=      Get From Dictionary    ${json}    id
    Set Global Variable    ${VLANGROUP_ID}

Create Mac address assigned to VLAN
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${payload}       Create Dictionary    mac_address    90:B0:E9:53:91:EF    status      active     end_date     2024-10-30     description     robot_testing     assigned_object_id     ${VLAN_ID}      assigned_object_type     vlan      
    ${response}=  POST  ${BASE_URL}${MAC_ENDPOINT}    json=${payload}    headers=${HEADERS}  expected_status=201
    ${json}  Set Variable    ${response.json()}
    Convert to Dictionary    ${json}
    ${MACADDRESS_VLAN_ID}=      Get From Dictionary    ${json}    id
    Set Global Variable    ${MACADDRESS_VLAN_ID}

Create Test Unique Mac address constraint
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${payload}       Create Dictionary    mac_address    90:B0:E9:53:91:EF    status      active     end_date     2024-10-30     description     robot_testing_vlan     assigned_object_id     1      assigned_object_type     vlan      
    ${response}=  POST  ${BASE_URL}${MAC_ENDPOINT}    json=${payload}    headers=${HEADERS}  expected_status=400

Create Mac address assigned to VLAN Group
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${payload}       Create Dictionary    mac_address    90:B0:E9:53:92:EF    status      active     end_date     2024-10-30     description     robot_testing_vlan_group     assigned_object_id     ${VLANGROUP_ID}      assigned_object_type     vlangroup      
    ${response}=  POST  ${BASE_URL}${MAC_ENDPOINT}    json=${payload}    headers=${HEADERS}  expected_status=201
    ${json}  Set Variable    ${response.json()}
    Convert to Dictionary    ${json}
    ${MACADDRESS_VLANGROUP_ID}=      Get From Dictionary    ${json}    id
    Set Global Variable    ${MACADDRESS_VLANGROUP_ID}

Create Mac address assigned to wrong Object
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${payload}       Create Dictionary    mac_address    90:B0:E9:53:93:EF    status      active     end_date     2024-10-30     description     robot_testing_vlan_group     assigned_object_id     1      assigned_object_type     wrong_obj      
    ${response}=  POST  ${BASE_URL}${MAC_ENDPOINT}    json=${payload}    headers=${HEADERS}  expected_status=400

Delete Mac Address assigned to VLAN
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${response}=  DELETE  ${BASE_URL}${MAC_ENDPOINT}${MACADDRESS_VLAN_ID}    headers=${HEADERS}  expected_status=204

Delete Mac Address assigned to VLAN Group
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${response}=  DELETE  ${BASE_URL}${MAC_ENDPOINT}${MACADDRESS_VLANGROUP_ID}    headers=${HEADERS}  expected_status=204

Delete VLAN
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${response}=  DELETE  ${BASE_URL}${VLAN_ENDPOINT}${VLAN_ID}    headers=${HEADERS}  expected_status=204

Delete VLAN Group
    ${HEADERS}       Create Dictionary    Content-Type    ${CONTENT_TYPE}    Authorization    ${TOKEN}
    ${response}=  DELETE  ${BASE_URL}${VLANGROUP_ENDPOINT}${VLANGROUP_ID}    headers=${HEADERS}  expected_status=204
