FROM netboxcommunity/netbox:v3.6.9-2.7.0

RUN mkdir /opt/netbox/plugins
COPY requirements.txt /opt/netbox/plugins
COPY netbox_configuration/plugins.py /etc/netbox/config/plugins.py
COPY netbox_configuration/extra.py /etc/netbox/config/extra.py
COPY netbox_configuration/field_choices.py /etc/netbox/config/field_choices.py
COPY netbox_configuration/entrypoint.sh /usr/bin/netbox_plugin_entrypoint.sh
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN pip install -r /opt/netbox/plugins/requirements.txt
RUN rm -rf /opt/netbox/plugins

CMD ["/usr/bin/netbox_plugin_entrypoint.sh"]
